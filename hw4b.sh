#!/usr/bin/env bash

if [ ! -f output4-3.txt ]
then
    python2.7 model.py $1 $2
fi

while [ ! -f output4-3.txt ]
do
    sleep 2
done

cat output4-3.txt
cat output4-4.txt