#!/usr/bin/env python2.7

import codecs
import os
from glob import glob
import BeautifulSoup as bs # for SGML parsing
from nltk.tokenize import word_tokenize as wtokenize
from math import log

def good_turing(c, k, Nmatrix):
    """
    Given a frequency, smoothing threshold, and a table
    of frequencies of frequencies, return a smoothed count
    for counts of the given frequency (i.e., return c* given
    c, k, and a table of Nc from c = 0 to k + 1).
    """
    N_1 = float(Nmatrix.get(1, 0))
    N_kplus1 = Nmatrix.get(k + 1, 0)
    N_c = float(Nmatrix.get(c, 0))
    N_cplus1 = Nmatrix.get(c + 1, 0)
    return ((c + 1) * N_cplus1 / N_c - c * (k + 1) * N_kplus1 / N_1) / (1 - (k + 1) * N_kplus1 / N_1)

def gt_smooth_counts(k, ngram_fq, unigram_len, n):
    """
    Given a threshold, ngram frequency dict, number of seen 
    unigrams, and ngram length, return a tuple containing a
    table of frequencies of frequencies for the given ngrams,
    and a table of smoothed counts for the ngrams.
    """
    
    # create frequency of frequencies table, and calculate N (total number of ngrams)
    N = 0
    Nmatrix = {}
    for ngram in ngram_fq:
        c = ngram_fq[ngram]
        if c <= k + 1:
            Nmatrix[c] = Nmatrix.get(c, 0) + 1
        N += c
            
    # N_0 == number possible ngrams minus number of ngrams seen -- c(unseen ngrams)
    Nmatrix[0] = unigram_len ** n - len(ngram_fq)
    N += Nmatrix[0]

    # smoothed counts
    c_star = {}

    if n > 1:
        c_star[0] = Nmatrix[1] / float(Nmatrix[0])
    elif n == 1:
        c_star[0] = Nmatrix[1] / float(N)
    for c in range(1, k + 1):
        Nmatrix[c] = Nmatrix.get(c, 0)
        c_star[c] = good_turing(c, k, Nmatrix)

    return (Nmatrix, c_star)

def extract_contents(tag):
    """
    Recursivey extract non-SGML content from SGML tag.
    """
    contents = []
    for c in tag.contents:
        if not isinstance(c, bs.NavigableString):
            c = extract_contents(c)
        contents.append(c.strip())
    return '\n'.join(contents)

def process_test(tdir):
    """
    Given test directory, processes data from 
    files into tokens ready for testing.
    """
    if not os.path.isdir(tdir):
        exit("{0} could not be found.")
    else:
        test_tokens = []
        for fname in glob(os.path.join(tdir, '*.txt')):
            with codecs.open(fname, encoding='latin1') as f:
                test_tokens.extend(tokenize(f.read().strip()))
        return test_tokens

def tokenize(data):
    punct = ',.?!'
    data = wtokenize(data)
    tokenized = []
    # split off punctuation marks
    for datum in data:
        if len(datum) > 1 and datum[-1] in punct:
            tokenized.extend((datum[:-1].lower(), datum[-1]))
        elif not datum:
            continue
        else:
            tokenized.append(datum.lower())
    return tokenized

class LM:
    def __init__(self, ddir, subset=False):
        """
        Initialize an LM object. Generates a language model 
        using the data in ddir (all if bool(subset) == False, else
        only the data range of filenames determined by subset).
        The model uses add-one smoothing by default, and can
        also use Good Turing smoothing (with a threshold of k=5) 
        if smooth == "good-turing". The model is based on n-grams
        -- bigrams by default.
        """
        
        if not os.path.isdir(ddir):
            exit("{0} could not be found".format(ddir))
        else:
            basename = 'afp'
            if subset: # limit filename pattern to select desired file subset
                basename += subset
            glob_p = os.path.join(ddir, "{}{}".format(basename, "*.sgm")) # filename pattern

            parser = bs.BeautifulStoneSoup()

            # extract content from SGML (uses BeautifulSoup module),
            # get token counts file by file

            self.token_fq = {}
            self.bigram_fq = {}

            self.UNK = "<UNK>"

            prev_tok = '' # stores the token seen in the previous iteration for reducing memory footprint
            for fname in sorted(glob(glob_p)): # iterate through all files matching filename pattern
                with codecs.open(fname, encoding='latin-1') as f:
                    parser.feed(f.read().strip())

                for tag in parser.fetch(['preamble', 'text']):
                    contents = extract_contents(tag)
                    contents = tokenize(contents)
                    for token in contents:
                        if token not in self.token_fq: # check if token is new
                            self.token_fq[token] = 0 # make sure we know token has been seen
                            token = self.UNK
                        self.token_fq[token] = self.token_fq.get(token, 0) + 1
                        if prev_tok:
                            bigram = prev_tok + ' ' + token
                            self.bigram_fq[bigram] = self.bigram_fq.get(bigram, 0) + 1
                        prev_tok = token

            # create probability matrix, using appropriate smoothing technique

            self.V = len(self.token_fq.keys()) # number of types

    def bigram_logprob_addone(self, bigram):
        """
        Return add-one-smoothed log probs of a bigram
        given a language model (self).
        """
        u1, u2 = bigram.split()
        c_bigram = float(self.bigram_fq.get(bigram, 0) + 1)
        c_u1 = self.token_fq[u1] + self.V
        return log(c_bigram/c_u1, 2)

if __name__ == '__main__':
    import sys

    if len(sys.argv) < 3:
        exit("Missing arguments")
    else:
        data_dir = sys.argv[1]
        test_dir = sys.argv[2]

        data_subset = "96"

        # models
        lm_full = LM(os.path.expanduser(data_dir), subset=False)
        lm_part = LM(os.path.expanduser(data_dir), subset=data_subset)

        models = {'all': lm_full, data_subset: lm_part}
        smoothings = ('add-1', 'good-turing')

        test_data = process_test(test_dir)
        n_bigrams = len(test_data) - 1 # number of bigrams in test data

        ### wrap this in a loop over the two available LMs ###

        output_counter = 1

        # output file for first table
        part_1 = "output4-0.txt"
        part_1_out = open(part_1, 'w')

        # calculate perplexity
        for smoothing in smoothings:
            for model_name in sorted(models.keys()):
                lm = models[model_name]
                if len(lm.token_fq) == 0:
                    print "something is wrong"
                    continue
                if smoothing == 'add-1':
                    neg_avg_logprob = 0
                    for i in range(n_bigrams):
                        u1 = test_data[i]
                        u2 = test_data[i+1]
                        # check if tokens are unseen in model
                        if lm.token_fq.get(u1, 0) == 0:
                            u1 = lm.UNK
                        if lm.token_fq.get(u2, 0) == 0:
                            u2 = lm.UNK
                        bigram = u1 + ' ' + u2
                        neg_avg_logprob += lm.bigram_logprob_addone(bigram)
                    neg_avg_logprob *= (-1 / float(n_bigrams))
                    part_1_out.write("LM {0} - {1}, {2}\t\t{3:.2f}".format(output_counter,
                                                                           model_name,
                                                                           smoothing.replace('-', ' ').title(),
                                                                           2 ** neg_avg_logprob) +
                                     "\n")

                elif smoothing == 'good-turing':
                    k = 5 # good turing threshold

                    Nmatrix_bi, c_star_bi = gt_smooth_counts(k, lm.bigram_fq, len(lm.token_fq), 2)
                    Nmatrix_uni, c_star_uni = gt_smooth_counts(k, lm.token_fq, len(lm.token_fq), 1)

                    neg_avg_logprob = 0
                    for i in range(n_bigrams):
                        u1 = test_data[i]
                        u2 = test_data[i+1]
                        # check if tokens are unseen in model
                        if lm.token_fq.get(u1, 0) == 0:
                            u1 = lm.UNK
                        if lm.token_fq.get(u2, 0) == 0:
                            u2 = lm.UNK
                        bigram = u1 + ' ' + u2
                        c_bi = float(lm.bigram_fq.get(bigram, 0))
                        c_uni = float(lm.token_fq.get(u1, 0))
                        if c_bi <= k: # get c* for bigram
                            c_bi = c_star_bi[c_bi]
                        if c_uni <= k: # get c* for bigram
                            c_uni = c_star_uni[c_uni]
                        
                        neg_avg_logprob += log(c_bi / c_uni, 2)
                    
                    neg_avg_logprob *= (-1 / float(n_bigrams))
                    part_1_out.write("LM {0} - {1}, {2}\t\t{3:.2f}".format(output_counter,
                                                                           model_name,
                                                                           smoothing.replace('-', ' ').title(),
                                                                           2 ** neg_avg_logprob) +
                                     "\n")

                    outfile = 'output4-{0}.txt'.format(output_counter)
                    
                    with open(outfile, 'w') as out:
                        out.write('LM {0} - {1}\n'.format(output_counter, smoothing.replace('-', ' ').title()))
                        out.write('\t\t'.join(('c', 'Nc', 'c*')) + '\n')
                        for c in Nmatrix_bi:
                            if c <= k: # omit N_k+1
                                out.write('\t\t'.join(str(num) for num in (c, Nmatrix_bi[c], '{:.2f}'.format(c_star_bi[c]))) + '\n')

                output_counter += 1
